<!--La carga de la hoja de estilos y del js luego habrá que quitarla cuando esto esté en el marco final-->
<!--<html lang="es">
    <head>
        <link href="../css/bootstrap.css" rel="stylesheet">
    </head>-->
    <?php
//incluyo la función para conectarme a la BBDD
    //include('../funciones.php');
    $mysqli = conectaLi();
    
 function convierteDeComas($listaRespuestas){
     return explode( ",", $listaRespuestas);
 }
 
//esto luego lo cambiamos por el usuario que esté logeado
    $id_usuario = 1;
    $test_elegido = 1;
    
    $query_random_enun = $mysqli->query("SELECT * FROM pregunta ");
    $num_preguntas = $query_random_enun->num_rows;

//guardo todas las respuestas en un array porque es más fácil trabajar luego con javascript
    $preguntas = array();
    for ($a = 0; $a < $num_preguntas; $a++) {
        $sqlrow = $query_random_enun->fetch_array();
        $id_pregunta = $sqlrow['Id_pregunta'];
        $preguntas [$id_pregunta][0] = $sqlrow['Enunciado'];
        $preguntas [$id_pregunta][1] = $sqlrow['R1'];
        $preguntas [$id_pregunta][2] = $sqlrow['R2'];
        $preguntas [$id_pregunta][3] = $sqlrow['R3'];
        $preguntas [$id_pregunta][4] = $sqlrow['R4'];
        $preguntas [$id_pregunta][5] = $sqlrow['R5'];
        $preguntas [$id_pregunta][6] = $sqlrow['R6'];
        $preguntas [$id_pregunta][7] = $id_pregunta;
    }

    //guardo las respuestas en un array, para luego ir actualizando la BBDD
    
    $respuestas = array();
    $query_tests = $mysqli->query("SELECT * FROM TestsRealizados where id_test = '$test_elegido' ");
    $sqlrow = $query_tests->fetch_array();
    //obtiene la lista de respuestas separadas por comas
    $listaRespuestas = $sqlrow['respuestas_dadas'];
    //convierte la lista de respuestas a array
    $respuestas = convierteDeComas($listaRespuestas);
    //lee las preguntas concretas del test y las guarda en una lista
    $listaPreguntas = convierteDeComas($sqlrow['preguntas_elegidas']);
    //print_r( $listaPreguntas);
    $preguntasElegidas = array();
    for ($a=0; $a<count($listaPreguntas)-1; $a++){
        $preguntasElegidas[$a] = $preguntas[$listaPreguntas[$a]];
    }
    
    
    
    //a esta función le pasas la respuesta que quieres poner con php en la pantalla, y el número de respuesta
    //lo que hace es escribir el texto de la pregunta en la fila correspondiente
    function muestraRespuesta($respuesta, $numRespuesta) {
        if ($respuesta != "") {
            echo '<tr><td style="padding:5px; padding-left:30px;" id="respuesta' . $numRespuesta . '"><p><input type=checkbox id="check' . $numRespuesta . '">  ' . $respuesta . '</p></td></tr>';
        }
    }
    
    
    
    ?>
<style>
    #temp{
        
    }
    tr {
width: 100%;
display: inline-table;
table-layout: fixed;
}
@media (min-width:768px){ 
    table{
     height:600px;              
     display: -moz-groupbox;    
    }
    tbody{
      overflow-y: scroll;      
      height: 600px;           
      width: 100%;
      position: absolute;
    }
}

@media (max-width:768px){
     table{
     height:1200px;              
     display: -moz-groupbox;    
    }
    tbody{
      overflow-y: scroll;      
      height: 400px;           
      width: 100%;
      position: absolute;
    }   
}
.dCode{
    font-family: 'Courier New';
}

.dNoWrap{
    white-space: pre;
}


</style>
<?php 
    //require ('temporizador.php');
?>

<div style="padding-top: 100px;">
    <div class="container-fluid">
       
        <div class="row" >
            <div id="testTerminado" class="modal fade" tabindex="-1" role="dialog">
                        <div class="modal-dialog modal-lg largoModal" style="">
                            <div class="modal-content">
                                <div class="modal-header">
                                    
                                    <h3 style="color: red; text-align: center;" >TIEMPO TERMINADO</h3>
                                </div>
                                <div class="modal-body">
                                    <div class="row" >
                                        <div class="col-xs-12">

                                            <div style="text-align: center; font-size: 20px; color: black; ">Ha terminado el tiempo del examen, podrá consultar su resultado a continuación.</div>
                                            <br><br>
                                            
                                            <br><br>
                                            <button class="btn btn-primary btn-block" onclick="muestraResultados();"> MOSTRAR RESULTADOS <span class="glyphicon glyphicon-triangle-right"></span></button>

                                        </div>
                                        <div class="col-xs-5">
                                            <div id="observacionesLista"></div>
                                            <div id="observacionesListaOculta" hidden></div> <!--  -->
                                        </div>

                                    </div>

                                </div>
                                <div class="modal-footer">
                                </div>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->
                    
        <div class="modal fade" id="mostrarResultados" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <!--equis(para cerrar la emergente)-->
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <!--titulo-->
                        <h4 class="modal-title" id="myModalLabel">RESULTADO DEL EXAMEN</h4>
                    </div>
                    <div class="modal-content" align="justify"><!--Aqui es donde va el contenido-->

                       <table id="recargar" class="table table-striped">
<?php
echo '<tr>';
echo '<th>Test</th>';
echo '<th></th>';
echo '<th>Fecha</th>';
echo '<th></th>';
echo '<th>Puntuación</th>';
echo '</tr>';
for ($i = 0; $i < 1; $i++) {

    echo '<tr>';
    
     echo '<td>Test 1</td>';
 echo '<td>20/3/2016</td>';
 echo '<td>75%</td>';
    echo '<tr>';
}
?>
</table>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div><!--fin Modal Aceptar Alumno-->
            
        <table id="tabla" class="table  table-condensed table-bordered" style="background-color: #999; color: whitesmoke;">
            <tr><td  style="padding:30px;"> <span id="numeroPregunta"> Pregunta 1 de <?php echo count($listaPreguntas); ?> </span>   
                    <div>
                    <button id="siguiente" class="btn btn-info" onclick="siguiente();">SIGUIENTE</button> 
                    <button  class="btn btn-warning" id="tiempo" ></button>
                    
                    <button style="background-color: #999; position: absolute; left: 40%; border-style: none; font-size: 22px;"><label style="background-color: #bce8f1; color: red;" id="alerta1Minuto"></label></button>
                    <button class="btn btn-primary" id="tiempoTotal" style="float: right;" ></button>
                    </div>
                    
                </td></tr>
            <?php
            $a = 0;
            echo '<tr><td  id="pregunta" style="padding:30px; ">' . $preguntasElegidas [$a][0] . '</td></tr>';
            for ($i = 1; $i < 7; $i++) {
                muestraRespuesta($preguntasElegidas [$a][$i], $i);
            }
           
            ?>
           <tr><td  style="padding:30px;"></td></tr> 
        </table>
        </div>
    </div>
</div>
    <div  id="guarda"></div>


<!--esta carga de script de jquery y bootstrap hay que borrarla cuando pongamos este código dentro del marco final-->
<!--    <script src="../js/jquery-3.1.1.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>-->
    <script>
            var preguntas = <?php echo json_encode($preguntasElegidas); ?>;
            var respuestas = <?php echo json_encode($respuestas); ?>;
            var max_preguntas = <?php echo json_encode($num_preguntas); ?>;
            var id_test = <?php echo json_encode($test_elegido); ?>;
            var num_pregunta = 0;
            var numeroDePreguntas = <?php echo count($listaPreguntas); ?>;
            var tiempo_transcurrido;
            var tiempo_total = 8;//5400 es igual a 1 hora y 30 minutos
            var segundosTranscurridos = 0;
            
        
        function reseteaTemporizador(){
            segundosTranscurridos = 0;
            $('#tiempo').html("00:00:00");
            clearInterval(tiempo_transcurrido);
            tiempo_transcurrido = setInterval(function(){
                segundosTranscurridos ++;
                horas = Math.floor(segundosTranscurridos / 3600);                
                totalSegundos = segundosTranscurridos % 3600;
                minutos = Math.floor(totalSegundos / 60);
                segundos = totalSegundos % 60;
                if (horas   < 10) {horas   = "0"+horas;}
                if (minutos < 10) {minutos = "0"+minutos;}
                if (segundos < 10) {segundos = "0"+segundos;}
                transcurrido = horas.toString() + ":" + minutos.toString() + ":" + segundos.toString();
                $('#tiempo').html("     "+transcurrido+"  ");
            }, 1000);
        }
        
          
        $( document ).ready(function() {
            
            escribePreguntas();
            reseteaTemporizador();
            tiempo_atras = setInterval(function(){
                tiempo_total --;                              
                horas = Math.floor(tiempo_total / 3600);                
                totalSegundos = tiempo_total % 3600;
                minutos = Math.floor(totalSegundos / 60);
                segundos = totalSegundos % 60;
                if (horas   < 10) {horas   = "0"+horas;}
                if (minutos < 10) {minutos = "0"+minutos;}
                if (segundos < 10) {segundos = "0"+segundos;}
                restante = horas.toString() + ":" + minutos.toString() + ":" + segundos.toString();
                $('#tiempoTotal').html("     "+restante+"  ");  
                if (restante == "00:00:05"){
                $('#alerta1Minuto').html("¡¡¡¡¡TE QUEDA 1 MINUTO!!!!!")
            }
            if (restante == "00:00:03"){
                $('#alerta1Minuto').html("")
                
            }
            if (restante == "00:00:00"){
                $('#testTerminado').modal('show');
                clearInterval(tiempo_atras);
                clearInterval(tiempo_transcurrido);
                $('#tabla').hide();
                
            }
            
            }, 1000); 
            


        });
        
        function muestraResultados(){
            $('#testTerminado').hide();
            $('#mostrarResultados').modal('show');
            
        }
        
        function volverInicio(){
            $('#mostrarResultados').hide();
            $('#central').load("index.php");
        }

            //La función siguiente se encarga de manejar lo que pasa cuando se pulsa el botón siguiente
            //tiene que: 
            
            //  guardar las respuestas que ha dado el usuario en la base de datos
            //  //  cambiar la pregunta y si están en blanco no ponerlas
            //  chequear si ha terminado la partida, si quiere pausar, recuperar un test empezado etc
            
        function siguiente() {
            
            reseteaTemporizador();
            if (num_pregunta < numeroDePreguntas -1){

                //lee los ticks de las preguntas actuales y los inserta en la BBDD
                //el peazo código de actualizaRespuestasTest es de Hector
                var r = '';
                for (var i = 1; i < 7; i++) {
                    if(leeTick(i)){ r = r + '1'; }
                    else {r = r + '0';}
                }
                respuestas[num_pregunta] = r;
                var textoRespuestas = respuestas.toString()
                $('#guarda').load('p03tests/actualizaRespuestasTest.php',{
                    id_test : id_test,
                    respuestas_dadas : textoRespuestas
                });
                num_pregunta++;
                escribePreguntas();
                //console.log(respuestas);
            }
 
                
        }
            
            
            
            
            
        function escribePreguntas(){
                
            //pone la pregunta correspondiente
            $('#pregunta').html( preguntas[num_pregunta][0] );
            $('#numeroPregunta').html( "Pregunta " + (num_pregunta+1) + " de " + numeroDePreguntas + "  id: " + preguntas[num_pregunta][7]+ "  ");
            //pone cada respuesta en su sitio. Si está en blanco, lo borra por si hubiera otra respuesta anterior
            for (var i = 1; i < 7; i++) {
                if (preguntas[num_pregunta][i] != "") {
                    $('#respuesta' + i).html('<tr><td  id="respuesta' + i + '"><span><input type=checkbox id="check' + i + '">  ' + preguntas[num_pregunta][i] + '</span></td></tr>');
                }
                else {
                    $('#respuesta' + i).html('');
                }
                if (respuestas[num_pregunta][i-1] == '1' ) {
                    marcaTicks(i);
                }

            }
        } 
        function mostrarResultados(){
            
    
        }
            
        function marcaTicks(i){
            $('#check' + i).prop('checked', true);
        }

        function leeTick(i){
            return ($('#check' + i).prop('checked'));
        }
            
            
            
            

    </script>




